package mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import rawparser.app.App;


public class Connector {
	private Connection conn;
	private Boolean connectionFlag;
	
	private String url;
	private String user;
	private String password;
	
	public Connector(String url, String user, String password) {
		this.conn = null;
		this.url = url;
		this.user = user;
		this.password = password;
		this.connectionFlag = false;
	}
	
	public void openConnection() {
        try {
	        DriverLoader.load();
	        
			conn = DriverManager.getConnection(url, user, password);
			
			if (!conn.isClosed()) {
				App.logger.info("Connected to MySQL.");
			}
			
			connectionFlag = true;
        } catch (SQLException ex) {
            App.logger.warn("SQLException: " + ex.getMessage());
            App.logger.warn("SQLState: " + ex.getSQLState());
            App.logger.warn("VendorError: " + ex.getErrorCode());
            App.logger.warn("EXCEPTION TRACE [" + Connector.class.getName() + "]");
            
            connectionFlag = false;
        }
	}
	
	public Connection getConnection() {
		return connectionFlag ? conn : null;
	}
	
	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			App.logger.warn("Error closing MySQL connection");
			App.logger.warn("EXCEPTION TRACE ["
            		+ Connector.class.getName() + "]"
            		+ "[" + Thread.currentThread().getStackTrace()[2].getLineNumber() + "]");
		}
	}
}
