package mysql;

import rawparser.app.App;

public class DriverLoader {
	public static void load() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            String msg = ex.getMessage();
            App.logger.warn(msg);
            App.logger.warn("EXCEPTION TRACE [" + Connector.class.getName() + "]");
        }
    }
}
