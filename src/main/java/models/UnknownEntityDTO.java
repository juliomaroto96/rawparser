package models;

import java.util.Date;

public class UnknownEntityDTO {
	public Date date;
	
	public Integer logo_id;
	public Integer year;
	public Integer month;
	public Integer mention_id;
	public Integer language_id;
	public Integer continent_region_id;
	public Integer country_id;
	public Integer impact;
	public Integer impressions;
	public Integer lda_topic_number;
	public Integer author_country_id;
	public String gender_id;
	
	public String author;
	public String url;
	public String domain;
	public String page_id;
	
	@Override
	public String toString() {
		return "date=" + date + ", logo_id=" + logo_id + ", year=" + year + ", month=" + month
				+ ", mention_id=" + mention_id + ", language_id=" + language_id + ", continent_region_id="
				+ continent_region_id + ", country_id=" + country_id + ", impact=" + impact + ", impressions="
				+ impressions + ", ldaTopicNumber=" + lda_topic_number + ", author_country_id=" + author_country_id
				+ ", author=" + author + ", url=" + url + ", domain=" + domain + ", page_id=" + page_id + ", gender_id="
				+ gender_id;
	}

}
