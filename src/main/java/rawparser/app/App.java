package rawparser.app;

import java.sql.Connection;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import dao.UnknownEntityDAO;
import mysql.Connector;
import parser.DataHandler;
import properties_reader.PropertiesReader;

public class App 
{
	// MYSQL DATABASE KEY PROPERTIES.
	private static final String APP_NAME = "[RAW_PARSER]";
	private static final String URL_PROP_DESCR = "url";
	private static final String USER_PROP_DESCR = "user";
	private static final String PASSWD_PROP_DESCR = "password";
	
	// DATASET FILEPATH KEY PROPERTY.
	private static final String DATASET_PATH_PROP_DESC = "input";
	private static final String DATASET_FILENAME_PROP_DESC = "dataset_filepath";
	
	// GLOBAL LOGGER ACCESS.
	public static Logger logger;
	
	// LOCAL PROPERTIES FILE READER. DEPENDENCY INYECTOR
	private static PropertiesReader pr;
	
	// LOCAL DATABASE CONNECTOR TO HANDLE APP CONNECTIONS WITH DB
	private static Connector connector;
	
    public static void main( String[] args )
    {
    	// Log4j global configuration
    	BasicConfigurator.configure();
    	logger = Logger.getLogger(APP_NAME);
    	
    	// Properties reader
        pr = PropertiesReader.getPropertiesReader();
        
        // Mysql connection
        configureMysqlConnection();
        Connection mysqlConnection = connector.getConnection();
        
        String datasetFilename = pr.getPropertyByName(DATASET_FILENAME_PROP_DESC);
        String datasetFilepathVal = DATASET_PATH_PROP_DESC + "/" + datasetFilename;
        DataHandler dh = new DataHandler(datasetFilepathVal, mysqlConnection);
        dh.processFile();
        
    }
    
    private static void configureMysqlConnection() {
    	String url = pr.getPropertyByName(URL_PROP_DESCR);
    	String user = pr.getPropertyByName(USER_PROP_DESCR);
    	String password = pr.getPropertyByName(PASSWD_PROP_DESCR);
    	
    	connector = new Connector(url, user, password); 	
    	connector.openConnection();
    }
}
