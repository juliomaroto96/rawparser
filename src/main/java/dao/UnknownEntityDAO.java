package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import models.UnknownEntityDTO;
import rawparser.app.App;

public class UnknownEntityDAO {
	private ArrayList<UnknownEntityDTO> dtoList;
	private Connection mysqlConnection;
	
	public UnknownEntityDAO(ArrayList<UnknownEntityDTO> dtoList, Connection mysqlConnection) {
		this.dtoList = dtoList;
		this.mysqlConnection = mysqlConnection;
	}
	
	public void insertAll() {
		dropTableIfExists();
		createTable();
		
		dtoList.forEach((item) -> {
			insertRow(item);
		});
		
		checkInsertionIntegrity();
		closeConnection();
	}
	
	private void dropTableIfExists() {
		String dropTableSQL = "DROP TABLE IF EXISTS example_entity;";
		
		executeStatementUpdate(dropTableSQL);

	}
	
	private void createTable() {
		String createTableSQL = "CREATE TABLE example_entity (" + 
				"  logo_id int(11) DEFAULT NULL," + 
				"  date datetime DEFAULT NULL," + 
				"  year int(11) DEFAULT NULL," + 
				"  month int(11) DEFAULT NULL," + 
				"  mention_id int(11) DEFAULT NULL," + 
				"  url varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL," + 
				"  domain varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL," + 
				"  page_id varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL," + 
				"  language_id int(11) DEFAULT NULL," + 
				"  continent_region_id int(11) DEFAULT NULL," + 
				"  country_id int(11) DEFAULT NULL," + 
				"  author varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL," + 
				"  author_country_id int(11) DEFAULT NULL," + 
				"  gender_id varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL," + 
				"  impact int(11) DEFAULT NULL," + 
				"  impressions int(11) DEFAULT NULL," + 
				"  lda_topic_number int(11) DEFAULT NULL" + 
				") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";
				
		executeStatementUpdate(createTableSQL);

	}
	
	private void insertRow(UnknownEntityDTO dto) {
		String insertTableSQL = "INSERT INTO example_entity"
				+ "(logo_id, date, year, month, mention_id, url, domain, page_id, language_id, "
				+ "continent_region_id, country_id, author, author_country_id, gender_id,"
				+ "impact, impressions, lda_topic_number) "
				+ "VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		
		PreparedStatement preparedStatement = null;
		
		try {
			preparedStatement = mysqlConnection.prepareStatement(insertTableSQL);
			
			preparedStatement.setObject(1, dto.logo_id);
			preparedStatement.setDate(2, new java.sql.Date(dto.date.getTime()));
			preparedStatement.setObject(3, dto.year);
			preparedStatement.setObject(4, dto.month);
			preparedStatement.setObject(5, dto.mention_id);
			preparedStatement.setString(6, dto.url);
			preparedStatement.setString(7, dto.domain);
			preparedStatement.setString(8, dto.page_id);
			preparedStatement.setObject(9, dto.language_id);
			preparedStatement.setObject(10, dto.continent_region_id);
			preparedStatement.setObject(11, dto.country_id);
			preparedStatement.setString(12, dto.author);
			preparedStatement.setObject(13, dto.author_country_id);
			preparedStatement.setString(14, dto.gender_id);
			preparedStatement.setObject(15, dto.impact);
			preparedStatement.setObject(16, dto.impressions);
			preparedStatement.setObject(17, dto.lda_topic_number);
			
			preparedStatement.executeUpdate(); 
			preparedStatement.close();
		} catch (SQLException e) {
			System.out.println(dto.author_country_id);
		}
		
	}
	
	private void closeConnection() {
		try {
			mysqlConnection.close();
			
			App.logger.info("MySQL Connection closed.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void checkInsertionIntegrity() {
		String countRowsSQL = "SELECT COUNT(1) AS count FROM rawparser.example_entity";
		
		try {
			Statement e = mysqlConnection.createStatement();
			ResultSet rs = e.executeQuery(countRowsSQL);
			
			rs.next();
			Integer count = rs.getInt("count");
			
			if (count == dtoList.size()) {
				App.logger.info(count + " records inserted. Database integrity insertion [OK]");
			} else {
				App.logger.info(count + " records inserted. Database integrity insertion [FAIL]");
			}

			rs.close();
			e.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void executeStatementUpdate(String statement) {
		try {
			Statement e = mysqlConnection.createStatement();
			e.executeUpdate(statement);
			e.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
}
