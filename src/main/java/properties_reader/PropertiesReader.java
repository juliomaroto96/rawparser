package properties_reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import rawparser.app.App;

public class PropertiesReader {
	
	private final static String PROPERTIES_FILEPATH = "config/app.properties";
	private final static String EQUALS_TOKEN = "=";
	private final static String DOUBLE_QUOTES_SCAPED = "\"";

	private Map<String, String> properties;
	private BufferedReader br;
	
	// SINGLETON IMPLEMENTATION
	private static PropertiesReader pr;
	
	public static PropertiesReader getPropertiesReader() {
		if (null == pr) {
			pr = new PropertiesReader();
		}
		
		return pr;
	}
	
	private PropertiesReader() {
		openFile();
		readFile();
		closeFile();
	}
	
	private void openFile() {
		br = null;
		
		try {
			br = new BufferedReader(new FileReader(PROPERTIES_FILEPATH));
		} catch (FileNotFoundException e) {
			App.logger.warn("File has not be found.");
		}
	}
	
	private void readFile() {
		properties = new HashMap<String, String>();
		
		try {
			String currentLine = br.readLine();
			
			while ( null != currentLine) {
				
				if ( -1 != currentLine.indexOf(EQUALS_TOKEN) ) {
					int equalsTokenPointer = currentLine.indexOf(EQUALS_TOKEN);
					
					String key = currentLine.substring(0, equalsTokenPointer);
					key = key.replaceAll(DOUBLE_QUOTES_SCAPED, "");
					key = key.replaceAll(EQUALS_TOKEN, "");
					key = key.trim();
					
					String value = currentLine.substring(equalsTokenPointer);
					value = value.replaceAll(DOUBLE_QUOTES_SCAPED, "");
					value = value.replaceAll(EQUALS_TOKEN, "");
					value = value.trim();
										
					String searchedVal = properties.get(key);
					if (null == searchedVal || searchedVal.isEmpty()) {
						properties.put(key, value);
					}
					
				}
				
				currentLine = br.readLine();
			}
		} catch (IOException e) {
			App.logger.warn("I/O Error. Please check your hard devices to be connected and do not manipulate the app configuration file.");
		}
	}

	private void closeFile() {
		try {
			br.close();
		} catch (IOException e) {
			App.logger.warn("File cannot be closed");
		}
	}
	
	public String getPropertyByName(String propertyName) {
		return properties.get(propertyName);
	}
	
}
