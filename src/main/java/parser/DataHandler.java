package parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import dao.UnknownEntityDAO;
import models.UnknownEntityDTO;
import rawparser.app.App;
import java.sql.Connection;


public class DataHandler {

	private static final String COMMA_FILEPOINTER_CHAR = ",";
	private static final String EXPECTED_DATE_FORMAT = "MM/dd/yy";
	private static BufferedReader br;

	private ArrayList<UnknownEntityDTO> DTOList;
	private UnknownEntityDTO lastDTO;
	private Map<Integer, ArrayList<String>> dynamicMap;
	private Connection mysqlConnection;

	public DataHandler(String filepath, Connection mysqlConnection) {
		openFile(filepath);
		this.dynamicMap = new HashMap<Integer, ArrayList<String>>();
		this.mysqlConnection = mysqlConnection;
	}

	private void openFile(String filepath) {
		try {
			br = new BufferedReader(new FileReader(filepath));
		} catch (FileNotFoundException e) {
			App.logger.warn("File not found to handle. [" + DataHandler.class.getName() + "]");
		}
	}

	public void processFile() {
		getFieldMapping();
		processRows();
	}

	private void getFieldMapping() {
		String currentLine = "";

		try {
			currentLine = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		StringTokenizer st = new StringTokenizer(currentLine, COMMA_FILEPOINTER_CHAR);

		int fieldPosition = 0;
		while (st.hasMoreTokens()) {
			String fieldName = st.nextToken();

			ArrayList<String> fieldProperties = new ArrayList<String>();
			fieldProperties.add(fieldName);

			dynamicMap.put(fieldPosition, fieldProperties);

			fieldPosition++;
		}
	}

	private void processRows() {
		try {
			String currentLine = br.readLine();
			DTOList = new ArrayList<UnknownEntityDTO>();

			while (null != currentLine) {
				String[] columnArray = currentLine.split(COMMA_FILEPOINTER_CHAR);
				lastDTO = new UnknownEntityDTO();

				for (Integer i = 0; i < columnArray.length; i++) {
					Integer fieldPositionInRecord = i;
					ArrayList<String> fieldProperties = dynamicMap.get(fieldPositionInRecord);

					String fieldName = fieldProperties.get(0).toLowerCase().trim();

					String currentColumnValue = columnArray[i];
					currentColumnValue = currentColumnValue.trim();

					setColumnToModel(fieldName, currentColumnValue, lastDTO);
				}
					
				DTOList.add(lastDTO);
				currentLine = br.readLine();
			}
			
			br.close();
			UnknownEntityDAO dao = new UnknownEntityDAO(DTOList, mysqlConnection);
			dao.insertAll();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setColumnToModel(String fieldName, 
			String currentColumnValue, 
			UnknownEntityDTO ue) {
		
		if (isInteger(currentColumnValue)) {
			Integer intVal = Integer.parseInt(currentColumnValue);
			set(ue, fieldName, intVal);

		} else if (isThisDateValid(currentColumnValue, EXPECTED_DATE_FORMAT)) {
			SimpleDateFormat sdf = new SimpleDateFormat(EXPECTED_DATE_FORMAT);
			Date dateVal = getDateParsed(sdf, currentColumnValue);
			set(ue, fieldName, dateVal);
		} else if (currentColumnValue.length() >= 1){
			set(ue, fieldName, currentColumnValue);
		} else {
			set(ue, fieldName, null);
		}

	}

	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}

		return true;
	}

	private boolean isThisDateValid(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {
			Date date = sdf.parse(dateToValidate);
		} catch (ParseException e) {
			return false;
		}

		return true;
	}

	private Date getDateParsed(SimpleDateFormat sdf, String currentVal) {
		Date dateVal = null;

		try {
			dateVal = sdf.parse(currentVal);
		} catch (ParseException e) {
		}

		return dateVal;
	}

	private boolean set(Object object, String fieldName, Object fieldValue) {
		Class<?> clazz = object.getClass();
		while (clazz != null) {
			try {
				java.lang.reflect.Field field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(object, fieldValue);

				return true;
			} catch (NoSuchFieldException e) {
				clazz = clazz.getSuperclass();
			} catch (Exception e) {
				App.logger.warn("Cannot set value [" + fieldValue + "] for field: " + fieldName);
				System.out.println(lastDTO.toString());
				throw new IllegalStateException(e);
			}
		}
		return false;
	}

}
